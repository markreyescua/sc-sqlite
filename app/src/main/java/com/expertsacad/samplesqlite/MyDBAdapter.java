package com.expertsacad.samplesqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Mark Cua on 7/7/2016.
 */
public class MyDBAdapter {

    Context context;
    private myDBHelper dbHelper;
    private SQLiteDatabase db;

    private String DATABASE_NAME = "data";
    private int DATABASE_VERSION = 1;

    public MyDBAdapter(Context context) {
        this.context = context;
        dbHelper = new myDBHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        db = dbHelper.getWritableDatabase();
    }

    public void insertStudent(String name, int faculty) {
        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("faculty", faculty);
        db.insert("students", null, cv);
        db.insert("faculty", null, cv);
    }

    public ArrayList<String> selectAllStudents() {

        ArrayList<String> allStudents = new ArrayList<String>();
        Cursor cursor = db.query("faculty", null, null, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {

            do {
                String s = cursor.getString(0) + " : " + cursor.getString(1) + " : " + cursor.getString(2);
                allStudents.add(s);
            } while (cursor.moveToNext());

        }

        return allStudents;

    }

    public void deleteAllEngineers() {
        db.delete("students", "faculty=1", null);
    }

    public class myDBHelper extends SQLiteOpenHelper {

        public myDBHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String query = "CREATE TABLE students (id integer primary key autoincrement, name text, faculty integer);";
            db.execSQL(query);
            query = "CREATE TABLE faculty (id integer primary key autoincrement, name text, faculty integer);";
            db.execSQL(query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String query = "DROP TABLE IF EXISTS students;";
            db.execSQL(query);

            query = "DROP TABLE IF EXISTS faculty;";
            db.execSQL(query);

            onCreate(db);
        }

    }

}
