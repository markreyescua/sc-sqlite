package com.expertsacad.samplesqlite;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private MyDBAdapter dbAdapter;
    private ListView list;
    private Spinner faculties;
    private Button addStudent;
    private Button deleteEngineers;
    private EditText studentName;
    private String[] allFaculties = {"Engineering", "Business", "Arts"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        faculties = (Spinner) findViewById(R.id.faculties_spinner);
        studentName = (EditText) findViewById(R.id.student_name);
        addStudent = (Button) findViewById(R.id.add_student);
        deleteEngineers = (Button) findViewById(R.id.delete_engineers);
        list = (ListView) findViewById(R.id.student_list);

        addStudent.setOnClickListener(this);
        deleteEngineers.setOnClickListener(this);

        dbAdapter = new MyDBAdapter(MainActivity.this);
        dbAdapter.open();
        faculties.setAdapter(
                new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, allFaculties));
        loadList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_student:
                dbAdapter.insertStudent(studentName.getText().toString(),
                        faculties.getSelectedItemPosition() + 1);
                loadList();
                break;

            case R.id.delete_engineers:
                dbAdapter.deleteAllEngineers();
                loadList();
                break;

            default:
                break;
        }

    }

    private void loadList() {

        ArrayList<String> allStudents = new ArrayList<String>();
        allStudents = dbAdapter.selectAllStudents();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1, allStudents);
        list.setAdapter(adapter);
    }

}
